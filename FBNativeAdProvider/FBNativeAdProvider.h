//
//  FBNativeAdProvider.h
//  FBNativeAdProvider
//
//  Created by Yonatan Katz on 2/21/16.
//  Copyright © 2016 Yonatan Katz. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FBNativeAdProvider.
FOUNDATION_EXPORT double FBNativeAdProviderVersionNumber;

//! Project version string for FBNativeAdProvider.
FOUNDATION_EXPORT const unsigned char FBNativeAdProviderVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FBNativeAdProvider/PublicHeader.h>


